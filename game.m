function varargout = game(varargin)
%
% Kod MATLAB dla pliku game.fig
%
% AUTOR: Monika Nowakowicz 203314
% OPIS: Kod dla popularnej gry 2048
%
% UWAGI: Uruchomienie gry odbywa si� poprzez wywo�anie w oknie polece�
% MATLAB komendy 'game'.
%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @game_OpeningFcn, ...
                   'gui_OutputFcn',  @game_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before game is made visible.
function game_OpeningFcn(hObject, eventdata, handles, varargin)
resetfunction(handles);
handles.output = hObject;
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = game_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% --- Executes on button press in resetbutton.
function resetbutton_Callback(hObject, eventdata, handles)
resetfunction(handles);

% --- Executes on key press with focus on window
function figure1_KeyPressFcn(hObject, eventdata, handles)
key=eventdata.Key;
result=get(handles.resultmessage, 'String');
if (~isequal(result,'GAME OVER') && ~isequal(result,'GAME WON') && (strcmp(key,'leftarrow') || strcmp(key,'rightarrow') || strcmp(key,'uparrow') || strcmp(key,'downarrow')))
    [board,score]=getgamestate(handles);
        
    if strcmp(key,'leftarrow') 
        [newBoard,score] = moveleft(board,score);
    elseif strcmp(key,'rightarrow')
        [newBoard,score] = moveright(board,score);
    elseif strcmp(key,'uparrow')
        [newBoard,score] = moveup(board,score);
    elseif strcmp(key,'downarrow')
        [newBoard,score] = movedown(board,score);
    end

    set(handles.score, 'String', num2str(score));

    [emptyFieldsBoard, emptyFieldsCount]=getemptyfieldsinfo(newBoard);
    if ~isequal(newBoard,board)
        if ~isempty(find(newBoard==2048))
            updateboard(handles, newBoard);
            set(handles.resultmessage, 'String', 'GAME WON');
        elseif emptyFieldsCount~=0
            newBoard=insertnewitem(emptyFieldsBoard, emptyFieldsCount, newBoard);
            updateboard(handles, newBoard);
        else 
            set(handles.resultmessage, 'String', 'GAME OVER');
        end
    else    
        if  emptyFieldsCount==0 && ismoveimpossible(newBoard, score)
            set(handles.resultmessage, 'String', 'GAME OVER');
        end
    end      
end
