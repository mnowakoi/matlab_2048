function cannotMove = ismoveimpossible(board, score)
% Funkcja sprawdza czy jest niemo�liwe podj�cie jakiegokolwiek skutecznego
% ruchu w grze. Je�eli mo�liwe jest podj�cie ruchu pozwalaj�cego na
% kontynuowanie gry zwraca false, w przeciwnym wypadku true.
%

[newBoard, ~] = moveup(board, score);
cannotMoveUp = isequal(newBoard,board); 
[newBoard, ~] = moveright(board, score);
cannotMoveRight = isequal(newBoard,board);
[newBoard, ~] = moveleft(board, score);
cannotMoveLeft = isequal(newBoard,board);
[newBoard, ~] = movedown(board, score);
cannotMoveDown = isequal(newBoard,board);

cannotMove = cannotMoveUp && cannotMoveRight && cannotMoveLeft && cannotMoveDown;