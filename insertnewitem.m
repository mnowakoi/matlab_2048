function newBoard = insertnewitem(emptyFieldsBoard, emptyFieldsCount, board)
% Funkcja wprowadzająca na planszę nowy element.
%
nextItemIndex=emptyFieldsBoard(randi(emptyFieldsCount));
board(nextItemIndex)=2;
newBoard=board;