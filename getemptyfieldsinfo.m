function [emptyFieldsBoard, emptyFieldsCount] = getemptyfieldsinfo(board)
% Funkcja zwracająca informacje o wolnych polach na planszy.
%
emptyFieldsBoard=find(board==0);
emptyFieldsCount=length(emptyFieldsBoard);