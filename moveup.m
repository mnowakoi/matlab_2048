function [newBoard,score] = moveup(board,initialScore)
% Funkcja odpowiadaj�ca za przemieszczenie warto�ci i zsumowanie po ruchu w
% g�r�.
%
boardSize=4;
score=initialScore;
newBoard=zeros(boardSize);
            for  i=1:(boardSize)
                for j=1:boardSize
                    newValue=board(j,i);
                    isLookingForNext=true;
                    while isLookingForNext && j<boardSize
                        if board(j+1,i)==0
                            j=j+1;
                        elseif board(j+1,i)~=newValue
                            isLookingForNext=false;
                        else
                            isLookingForNext=false;
                            newValue=newValue*2;
                            score=score+newValue;
                            board(j+1,i)=0;
                        end
                    end
                    index=1;
                    while newBoard(index, i)~=0
                        index=index+1;
                    end
                    newBoard(index, i)=newValue;                 
                end
            end