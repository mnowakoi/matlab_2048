function [board, score]=getgamestate(handles)
% Funkcja pobierająca z interfejsu użytkownika aktualny stan gry- stan
% planszy oraz aktualny winik uzyskany przez użytkownika.
%
boardSize=4;
board=zeros(boardSize);
score=str2double(get(handles.score,'String'));
for  i=1:(boardSize)
    for j=1:(boardSize)
        fieldValue=str2double(get(eval(['handles.f' num2str(i) num2str(j)]),'String'));
        if isnan(fieldValue)
            board(i,j)=0;
        else
            board(i,j)=fieldValue;
        end
    end
end