function [newBoard,score] = moveright(board,initialScore)
% Funkcja odpowiadająca za przemieszczenie wartości i zsumowanie po ruchu w
% prawo.
%
boardSize=4;
score=initialScore;
newBoard=zeros(boardSize);
            for  i=1:(boardSize)
                for j=boardSize:-1:1
                    newValue=board(i,j);
                    isLookingForNext=true;
                    while isLookingForNext && j>1
                        if board(i,j-1)==0
                            j=j-1;
                        elseif board(i,j-1)~=newValue
                            isLookingForNext=false;
                        else
                            isLookingForNext=false;
                            newValue=newValue*2;
                            score=score+newValue;
                            board(i,j-1)=0;
                        end
                    end
                    index=boardSize;
                    while newBoard(i, index)~=0
                        index=index-1;
                    end
                    newBoard(i,index)=newValue;                 
                end
            end