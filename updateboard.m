function updateboard(handles, board)
% Funkcja, kt�ra nanosi na widoczn� na UI plansz� uaktualniony stan gry.
%
boardSize = 4;
colors=[[1 226/255 240/255];[1 204/255 229/255];[1 153/255 204/255];[1 102/255 178/255];[1 51/255 153/255];[1 0 127/255];[204/255 0 102/255 ];[153/255 0 76/255];[102/255 0 51/255];[102/255 0 0]];
for  i=1:(boardSize)
    for j=1:(boardSize)
        if board(i, j)==0
            newValue='';
            color=[1 1 1];
        else
            newValue=num2str(board(i,j));
            if log2(board(i,j))>length(colors(:,1))
                color=colors(end,:);
            else
                color=colors(log2(board(i,j)),:);
            end
        end
% Missing eval function here would be really not elegant so I'm using it
% even if it was missing in the lecture
        set(eval(['handles.f' num2str(i) num2str(j)]),'String',newValue);
        set(eval(['handles.f' num2str(i) num2str(j)]),'BackgroundColor',color);
    end
end