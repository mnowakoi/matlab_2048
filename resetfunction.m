function resetfunction(handles)
% Funkcja pozwalająca zainicjalizować planszę do rozpoczęcia gry.
%
set(handles.resultmessage, 'String', '');
set(handles.score, 'String', num2str(0));

boardSize=4;
board=zeros(boardSize);
board(randi([1 boardSize^2]))=2;
[emptyFieldsBoard, emptyFieldsCount]=getemptyfieldsinfo(board);
board=insertnewitem(emptyFieldsBoard, emptyFieldsCount, board);

updateboard(handles, board)