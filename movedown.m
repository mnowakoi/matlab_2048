function [newBoard,score] = movedown(board,initialScore)
% Funkcja odpowiadaj�ca za przemieszczenie warto�ci i zsumowanie po ruchu w
% d�.
%
boardSize=4;
score=initialScore;
newBoard=zeros(boardSize);
            for  i=1:(boardSize)
                for j=boardSize:-1:1
                    newValue=board(j,i);
                    isLookingForNext=true;
                    while isLookingForNext && j>1
                        if board(j-1,i)==0
                            j=j-1;
                        elseif board(j-1,i)~=newValue
                            isLookingForNext=false;
                        else
                            isLookingForNext=false;
                            newValue=newValue*2;
                            score=score+newValue;
                            board(j-1,i)=0;
                        end
                    end
                    index=boardSize;
                    while newBoard(index, i)~=0
                        index=index-1;
                    end
                    newBoard(index, i)=newValue;                 
                end
            end