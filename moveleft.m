function [newBoard,score] = moveleft(board,initialScore)
% Funkcja odpowiadająca za przemieszczenie wartości i zsumowanie po ruchu w
% lewo.
%
boardSize=4;
score=initialScore;
newBoard=zeros(boardSize);
            for  i=1:(boardSize)
                for j=1:(boardSize)
                    newValue=board(i,j);
                    isLookingForNext=true;
                    while isLookingForNext && j<boardSize
                        if board(i,j+1)==0
                            j=j+1;
                        elseif board(i,j+1)~=newValue
                            isLookingForNext=false;
                        else
                            isLookingForNext=false;
                            newValue=newValue*2;
                            score=score+newValue;
                            board(i,j+1)=0;
                        end
                    end
                    index=1;
                    while newBoard(i, index)~=0
                        index=index+1;
                    end
                    newBoard(i,index)=newValue;                 
                end
            end